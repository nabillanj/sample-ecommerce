//
//  Product.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Product: Object {
    
    @objc dynamic var idProduct: String?
    @objc dynamic var nameProduct: String?
    @objc dynamic var descriptionProduct: String?
    dynamic var priceProduct: Double?
    @objc dynamic var imgProduct: String?
    
    convenience init(withJSON json: JSON) {
        self.init()
        idProduct = json["id_product"].stringValue
        nameProduct = json["name_product"].stringValue
        descriptionProduct = json["description_product"].stringValue
        priceProduct = json["price_product"].double
        imgProduct = json["img_product"].stringValue
    }
    
    override static func primaryKey() -> String? {
        return "idProduct"
    }

}
