//
//  Cart.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import Foundation
import RealmSwift

class Cart: Object {
    
    @objc dynamic var idCart: String?
    @objc dynamic var idProduct: String?
    
    override static func primaryKey() -> String? {
        return "idCart"
    }
}
