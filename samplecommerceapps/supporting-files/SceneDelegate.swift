//
//  SceneDelegate.swift
//  samplecommerceapps
//
//  Created by user171431 on 4/7/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UISceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            window = UIWindow(windowScene: windowScene)
        }
    }
}
