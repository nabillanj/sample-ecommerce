//
//  AppDelegate.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleSignIn
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

        setupNavigation()
        setupDatabaseConfiguration()
        return true
    }
       
    func setupDatabaseConfiguration() {
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {}
        })
        Realm.Configuration.defaultConfiguration = config
        
        _ = try! Realm()
    }
    
    func setupNavigation() {
        var vc: UIViewController?
        if AccessToken.current != nil || GIDSignIn.sharedInstance().hasPreviousSignIn() {
            vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "ProductViewController")
        } else {
            vc = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
        }
        let navigation = UINavigationController(rootViewController: vc!)
        self.window?.rootViewController = navigation
        self.window?.makeKeyAndVisible()
    }
}

