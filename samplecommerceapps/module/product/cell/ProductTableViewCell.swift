//
//  ProductTableViewCell.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit

protocol ProductTableViewDelegate {
    func onClickAddToCart(withIdProduct idProduct:String)
}

class ProductTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewAddToCart: UIView!
    
    var idProduct: String?
    var delegate: ProductTableViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickAddToCart))
        viewAddToCart.addGestureRecognizer(tapGesture)
    }
    
    @objc func onClickAddToCart() {
        delegate?.onClickAddToCart(withIdProduct: idProduct ?? "")
    }
    
    func bind(withProduct product: Product, isCart cart: Bool = false) {
        if cart {
            viewAddToCart.isHidden = true
        }
        imgProduct.image = UIImage(named: product.imgProduct ?? "")
        lblProductName.text = product.nameProduct
        lblProductDescription.text = product.descriptionProduct
        if let price = product.priceProduct {
            lblPrice.text = "Rp. \(price),00"
        }
    }
}
