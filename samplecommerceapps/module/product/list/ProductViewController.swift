//
//  ProductViewController.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit

class ProductViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var listOfProduct: [Product] = []
    private var viewModel: ProductViewModel? = ProductViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        title = "Dashboard"
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.register(ProductTableViewCell.nib, forCellReuseIdentifier: ProductTableViewCell.identifier)
        viewModel?.getListProduct()
        
        viewModel?.onShowError = { [weak self] error in
            self?.showAlertWithMessage(withDescription: error)
        }
        
        viewModel?.onShowSuccess = { [weak self] listProduct in
            self?.listOfProduct = listProduct
            self?.tableView.reloadData()
        }
    }
    
    @IBAction func onClickToCartView(_ sender: Any) {
        self.goToCartView()
    }
}

extension ProductViewController: ProductTableViewDelegate {
    func onClickAddToCart(withIdProduct idProduct: String) {
        showAlertWithMessage(withTitle: "Success!" , withDescription: "Success add to cart")
    }
}

extension ProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let productCell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.identifier, for: indexPath) as! ProductTableViewCell
        productCell.bind(withProduct: listOfProduct[indexPath.row])
        
        return productCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfProduct.count
    }
}

extension UIViewController {
    func goToCartView() {
        let vc = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "CartViewController")
        navigationController?.pushViewController(vc, animated: true)
    }
}
