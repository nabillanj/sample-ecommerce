//
//  CartViewController.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit

class CartViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    var listOfProduct: [Product] = []
    var viewModel: CartViewModel? = CartViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
       
    func setupUI() {
        title = "Cart"
        hideNavbar(isHidden: false)
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.register(ProductTableViewCell.nib, forCellReuseIdentifier: ProductTableViewCell.identifier)
        
        viewModel?.getListProduct()
        viewModel?.onShowError = { [weak self] error in
            self?.showAlertWithMessage(withDescription: error)
        }
        
        viewModel?.onShowSuccess = { [weak self] listProduct in
            self?.listOfProduct = listProduct
            self?.tableView.reloadData()
        }
    }
    
    // MARK: - Actions
    @IBAction func didClickCheckoutButton(_ sender: Any) {
        if listOfProduct.isEmpty {
            self.showAlertWithMessage(withDescription: "Product is Empty")
        } else {
            self.gotoPayment()
        }
    }
}

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let productCell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.identifier, for: indexPath) as! ProductTableViewCell
        productCell.bind(withProduct: listOfProduct[indexPath.row], isCart: true)
        
        return productCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfProduct.count
    }
}

extension UIViewController {
    func gotoPayment() {
        let vc = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentInvoiceViewController")
        navigationController?.pushViewController(vc, animated: true)    }
}
