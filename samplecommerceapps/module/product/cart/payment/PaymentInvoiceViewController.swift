//
//  PaymentInvoiceViewController.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit

class PaymentInvoiceViewController: BaseViewController {

    private var isValidDocument: Bool = false
    var viewModel: PaymentInvoiceViewModel? = PaymentInvoiceViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Actions
    
    @IBAction func onClickDoneButton(_ sender: Any) {
        if isValidDocument {
            self.goToProductList()
        }
    }
    
    @IBAction func onClickUploadFile(_ sender: Any) {
        
    }
    
}
