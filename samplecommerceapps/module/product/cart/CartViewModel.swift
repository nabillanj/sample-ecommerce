//
//  CartViewModel.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import Foundation

class CartViewModel: ProductProtocol {
    var onShowError: ((String) -> Void)?
    var onShowSuccess: (([Product]) -> ())?
    
    func getListProduct() {
        DispatchQueue.main.async {
            DatabaseHelper.shared.getAllListProduct { product in
                if product.isEmpty {
                    self.onShowError?("Failed to get product")
                } else {
                    self.onShowSuccess?(product)
                }
            }
        }
    }
}
