//
//  ProductProtocol.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import Foundation

protocol ProductProtocol {
    var onShowError: ((_ error: String) -> Void)? { get set }
    var onShowSuccess: ((_ product: [Product]) -> ())? { get set }
}
