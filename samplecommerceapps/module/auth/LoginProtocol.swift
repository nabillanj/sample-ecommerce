//
//  LoginProtocol.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import Foundation

protocol LoginProtocol {
    var onShowError: ((_ error: String) -> Void)? { get set }
    var onShowSuccess: (() -> ())? { get set }
}
