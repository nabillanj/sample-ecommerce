//
//  LoginViewController.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    var viewModel: LoginViewModel? = LoginViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        viewModel?.onShowError = { [weak self] error in
            self?.showAlertWithMessage(withDescription: error)
        }
        
        viewModel?.onShowSuccess = { [weak self] in
            self?.goToProductList()
        }
        
        hideNavbar()
    }
    
    // MARK: - Actions
    @IBAction func didClickLoginWithFacebook(_ sender: Any) {
        viewModel?.loginWithFacebook(withController: self)
    }
    
    @IBAction func didClickLoginWithGoogle(_ sender: Any) {
        viewModel?.loginWithGoogle(withController: self)
    }
}

extension UIViewController {
    func goToProductList() {
        let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "ProductViewController")
        navigationController?.pushViewController(vc, animated: true)
    }
}
