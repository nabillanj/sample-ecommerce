//
//  LoginViewModel.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import GoogleSignIn

class LoginViewModel: LoginProtocol {
    var onShowError: ((String) -> Void)?
    var onShowSuccess: (() -> ())?
    
    func loginWithFacebook(withController controller: UIViewController) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: controller) { [weak self] (result, error) in
                
            guard error == nil else {
                self?.onShowError!(error!.localizedDescription)
                return
            }
                
            guard let result = result, !result.isCancelled else {
                print("User cancelled login")
                return
            }
            
            AccessToken.current = result.token
            self?.onShowSuccess!()
        }
    }
    
    func loginWithGoogle(withController controller: LoginViewController) {
        let googleSignIn = GIDSignIn.sharedInstance()
        googleSignIn?.presentingViewController = controller
        googleSignIn?.clientID = "191173402069-hpus88g6d2e1rnocdgrt6a42etcbho82.apps.googleusercontent.com"
        googleSignIn?.delegate = controller
        googleSignIn?.signIn()
    }
}


extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error == nil else {
            print(error!.localizedDescription)
            return
        }
        self.goToProductList()
    }
}
