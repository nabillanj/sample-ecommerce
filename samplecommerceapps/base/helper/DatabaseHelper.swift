//
//  DatabaseHelper.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class DatabaseHelper {
    private var database:Realm
    static let shared = DatabaseHelper()
    
    private init() {
       database = try! Realm()
    }
    
    // Get List Product
    func getAllListProduct(completion: @escaping(_ listProduct: [Product]) -> Void) {
        if let url = Bundle.main.url(forResource: "product", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let json = try JSON(data: data)
                
                var listOfProduct: [Product] = []
                for productJson in json["data"].arrayValue {
                    try! database.write {
                        let product = Product(withJSON: productJson)
                        listOfProduct.append(product)
                    }
                }
                
                completion(listOfProduct)
            } catch {
                completion([])
            }
        }
    }
    
    // Get List Cart
    
    // Add Item
    
    // Delete Item
}
