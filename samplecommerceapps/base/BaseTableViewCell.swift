//
//  BaseTableViewCell.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

}
