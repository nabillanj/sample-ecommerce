//
//  BaseViewController.swift
//  samplecommerceapps
//
//  Created by user168426 on 4/3/20.
//  Copyright © 2020 nabilla. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func hideNavbar(isHidden hidden: Bool = true) {
        navigationController?.isNavigationBarHidden = hidden
    }
    
    func showAlertWithMessage(withTitle title: String = "Terjadi Kesalahan",
                              withDescription description: String) {
        let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
}
